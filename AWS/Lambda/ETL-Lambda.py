from diagrams import Diagram, Cluster, Edge
from diagrams.aws.compute import Lambda
from diagrams.aws.security import IAM
from diagrams.aws.storage import S3
from diagrams.aws.management import ParameterStore

with Diagram("AWS-ETL-Lamdba", show=False):
    with Cluster("Parameters"):
        Parameters = [ParameterStore("QueryParameter"), ParameterStore("DestinationName")]

    with Cluster("Role"):
        ExecutionRole = IAM("ExecutionRole")
        with Cluster("Policies"):
            Policies = [IAM("S3ListenPolicy"), IAM("S3PutPolicy"), IAM("LambdaLogPolicy"),
                        IAM("ParameterGetPolicy")]

    with Cluster("ExternalSource"):
        with Cluster("EventSource"):
            TriggerBucket = S3("TriggerBucket")
    with Cluster("EtlProcess"):
        with Cluster("ExtractTransofrm"):
            Lambda = Lambda("Lambda")
        with Cluster("Load"):
            DestinationBucket = S3("DestinationBucket")

    ExecutionRole - Edge(color="orange") - Policies
    ExecutionRole >> Edge(color="red", style="dashed") >> Lambda
    Parameters << Edge(color="purple", style="dashed") << Lambda
    TriggerBucket >> Edge(color="blue") >> Lambda >> Edge(color="blue") >> DestinationBucket
