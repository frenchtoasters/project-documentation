from diagrams import Diagram, Cluster, Edge
from diagrams.k8s.infra import ETCD, Master, Node
from diagrams.k8s.storage import StorageClass
from diagrams.aws.network import Route53
from diagrams.onprem.monitoring import Prometheus, Grafana
from diagrams.onprem.logging import LogStash
from diagrams.onprem.network import HAProxy

with Diagram("OpenShiftaaS", show=False):
    with Cluster("vCloud-Director"):
        with Cluster("Openshift"):
            with Cluster("Nodes"):
                with Cluster("User-vOrg"):
                    with Cluster("User-vDC"):
                        with Cluster("Workers-vApp"):
                            worker1 = Node("Worker1")
                            worker2 = Node("Worker2")
                            worker3 = Node("Worker3")
                            workers = worker1 - Edge(color="purple", style="dashed") - worker2 - \
                                      Edge(color="purple", style="dashed") - worker3

                with Cluster("Management-vOrg"):
                    with Cluster("Management-vDC"):
                        with Cluster("Control-Plane-vApp"):
                            master1 = Master("Master1")
                            master2 = Master("Master2")
                            master3 = Master("Master3")
                            etcd1 = ETCD("Etcd1")
                            etcd2 = ETCD("Etcd2")
                            etcd3 = ETCD("Etcd3")
                            masters = master1 - Edge(color="blue", style="dashed") - master2 - \
                                      Edge(color="blue", style="dashed") - master3
                            etcds = etcd1 - Edge(color="blue", style="dashed") - etcd2 - \
                                    Edge(color="blue", style="dashed") - etcd3

            cluster = etcds << Edge(color="blue", style="dashed") >> masters << Edge(color="violet", style="dashed") \
                      >> workers
            with Cluster("Storage-Classes"):
                PersistentStorage = StorageClass("Trident")
                clusterStorage = masters - Edge(color="orange", style="dashed") - PersistentStorage - \
                                 Edge(color="orange", style="dashed") - workers - \
                                 Edge(color="orange", style="dashed") - etcds

    with Cluster("External-Host"):
        Bastion = Node("Bastion-Host")

        with Cluster("Bastion-Services"):
            DNS = Route53("CoreDNS")
            Monitoring = Prometheus("Federated")
            Visualization = Grafana("Prometheus")
            Logging = LogStash("Logging")
            services = DNS - Edge(color="orange", style="dashed") - Monitoring - Edge(color="orange", style="dashed")\
                       - Visualization - Edge(color="orange", style="dashed") - Logging

        bastionServices = Bastion - Edge(color="orange", style="dashed") - services
    clusterServices = Bastion >> Edge(color="red", style="dashed") << masters << Edge(color="red", style="dashed") >> \
                      etcds << Edge(color="red", style="dashed") >> workers

    with Cluster("Cluster-Load-Balancer"):
        clusterLoadBalancer = HAProxy("etcds-masters-workers")
        clusterLB = clusterLoadBalancer << Edge(color="green", style="dashed") >> masters
        workerLB = clusterLoadBalancer << Edge(color="green", style="dashed") >> workers
        etcdLB = clusterLoadBalancer << Edge(color="green", style="dashed") >> etcds
