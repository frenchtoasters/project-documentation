from diagrams import Diagram, Cluster, Edge
from diagrams.k8s.podconfig import ConfigMap, Secret
from diagrams.k8s.network import Service, NetworkPolicy, Ingress
from diagrams.k8s.compute import Pod, ReplicaSet, Deployment
from diagrams.k8s.others import CRD
from diagrams.onprem.compute import Server
from diagrams.onprem.client import Users

with Diagram("ZabbixaaS", show=False):
    with Cluster("Cluster"):
        with Cluster("Namespace"):
            with Cluster("Server-Deployment"):
                with Cluster("server"):
                    ZabbixServer = Pod("zabbix-server")
                    with Cluster("server-configuration"):
                        ServerConfig = ConfigMap("zabbix_server.conf")
                        SSHkey = Secret("zabbix_ssh.pem")
                        config = ServerConfig - Edge(color="orange", style="dashed") - SSHkey
                        configuration = ZabbixServer - Edge(color="orange", style="dashed") - config
                with Cluster("database"):
                    ZabbixDatabase = Deployment("postgres")
                server = ZabbixServer >> Edge(color="violet", style="dashed") >> ZabbixDatabase

                with Cluster("frontend"):
                    ZabbixFrontend = ReplicaSet("zabbix-web-ngix")
                    frontend = ZabbixFrontend << Edge(color="violet", style="dashed") >> server

                with Cluster("network-configuration"):
                    ZabbixNetwork = NetworkPolicy("zabbix-network-policy")
                    ZabbixService = Service("zabbix-server-service")
                    ZabbixIngress = Ingress("zabbix-cluster.domain")
                    net = ZabbixService - Edge(color="orange", style="dashed") - ZabbixNetwork - \
                          Edge(color="orange", style="dashed") - ZabbixIngress

                with Cluster("database-crd"):
                    Database = CRD("operator-crd")
                    database = ZabbixDatabase - Edge(color="orange", style="dashed") - Database

    with Cluster("External-Server"):
        ZabbixProxy = Server("zabbix-proxy-mysql")
        proxyPath = ZabbixProxy >> Edge(color="crimson") >> ZabbixIngress >> Edge(color="crimson") >> ZabbixService >> \
                    Edge(color="crimson") >> ZabbixServer

    with Cluster("Access"):
        Admins = Users("admin-users")
        admins = Admins >> Edge(color="purple") >> ZabbixIngress >> Edge(color="purple") >> ZabbixService >> \
                 Edge(color="purple") >> ZabbixFrontend
