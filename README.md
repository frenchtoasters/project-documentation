# Documentation

This repo holds documentation on the things that I have built recently. 

## Lambda

* ETL-Lambda: this is an ETL process that when a `.csv` file is placed in the `TriggerBucket` and outputs it to the `DestinationBucket`.
* OpenshiftaaS: this describes an Openshift environment that is delivered As A Service via a combination of `Terraform` and a `vCloud Director` or other cloud environment.
* ZabbixaaS: this describes the Kuberentes deployment of a Zabbix environment running on an Openshift Kubernetes cluster deployed via `helm`.
* Alerting:
    * Cherwell: Direct integrations for creating Cherwell incidents via the RESTapi
    * PagerDuty: Direct integrations for creating PagerDuty Events V2 via the pypd python library

