from diagrams import Diagram, Cluster, Edge
from diagrams.custom import Custom
from diagrams.aws.compute import Lambda
from diagrams.onprem.monitoring import Prometheus

with Diagram("CherwellIncidentCreation", show=False):
    with Cluster("Cherwell-Integrations"):
        with Cluster("MonitoringSystem"):
            halcyon = Custom(icon_path="./Alerting/Cherwell/halcyon.png", label="halcyon-tranform")
            zabbix = Custom(icon_path="./Alerting/Cherwell/zabbix.png", label="zabbix-alertscript")
            prometheus = Prometheus("prometheues-service")

        with Cluster("Scheduled-Lambdas"):
            halcyonTransform = Lambda("TransformHalcyon")
            transformHal = halcyon << Edge(color="red") << halcyonTransform

        with Cluster("Serverless-Lambdas"):
            prometheusTransform = Lambda("TransformProm")
            transformProm = prometheusTransform << Edge(color="red") << prometheus

        with Cluster("Cherwell-Instance"):
            cherwell = Custom(icon_path="./Alerting/Cherwell/cherwell.png", label="cherwell")

        halCherwell = cherwell << Edge(color="purple") << halcyonTransform
        promCherwell = cherwell << Edge(color="purple") << prometheusTransform
        zabCherwell = cherwell << Edge(color="purple") << zabbix


