from diagrams import Diagram, Cluster, Edge
from diagrams.custom import Custom
from diagrams.aws.compute import Lambda
from diagrams.onprem.monitoring import Prometheus

with Diagram("PagerDutyEventCreation", show=False):
    with Cluster("PagerDuty-Integrations"):
        with Cluster("MonitoringSystem"):
            halcyon = Custom(icon_path="./Alerting/PagerDuty/halcyon.png", label="halcyon-tranform")
            zabbix = Custom(icon_path="./Alerting/PagerDuty/zabbix.png", label="zabbix-alertscript")
            prometheus = Prometheus("prometheues-service")

        with Cluster("Scheduled-Lambdas"):
            halcyonTransform = Lambda("TransformHalcyon")
            transformHal = halcyon << Edge(color="red") << halcyonTransform

        with Cluster("Serverless-Lambdas"):
            prometheusTransform = Lambda("TransformProm")
            transformProm = prometheusTransform << Edge(color="red") << prometheus

        with Cluster("Cherwell-Instance"):
            cherwell = Custom(icon_path="./Alerting/PagerDuty/cherwell.png", label="cherwell")

        with Cluster("PagerDuty-Instance"):
            pagerduty = Custom(icon_path="./Alerting/PagerDuty/pagerduty.png", label="cherwell")

        halPD = pagerduty << Edge(color="purple") << halcyonTransform
        promPD = pagerduty << Edge(color="purple") << prometheusTransform
        zabPD = pagerduty << Edge(color="purple") << zabbix

        cherwellPD = cherwell << Edge(color="green") << pagerduty
